import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.15 as Kirigami

Kirigami.ApplicationWindow {
    id: root

    pageStack {
        id: stackView
        initialPage: Qt.resolvedUrl('OpenDialogPage.qml')
        interactive: false
        globalToolBar.style: Kirigami.ApplicationHeaderStyle.Titles
    }

    footer: Kirigami.InlineMessage {
        id: message
        visible: false
        showCloseButton: true
        Layout.fillWidth: true

        function showError(message) {
            type = Kirigami.MessageType.Error
            text = message

            visible = true
        }

        function clear() {
            visible = false
            text = ''
        }
    }

    Connections {
        target: backend
        function onDatabaseOpened() {
            stackView.replace(Qt.resolvedUrl('EntriesPage.qml'))
        }
        function onDatabaseClosed() {
            stackView.replace(Qt.resolvedUrl('OpenDialogPage.qml'))
        }
    }
}
