import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtQml.Models 2.15
import Qt.labs.qmlmodels 1.0

import org.kde.kirigami 2.15 as Kirigami

import "components"

Kirigami.ScrollablePage {
    title: backend.databaseName

    header: ToolBar {
        RowLayout {
            spacing: 0

            Repeater {
                model: backend.model.selectionLevels
                delegate: RowLayout {
                    spacing: 0
                    Label {
                        text: '/'
                        visible: index > 0
                    }
                    ToolButton {
                        text: modelData
                        onClicked: backend.model.selectLevel(index)
                    }
                }
            }
        }
    }

    ListView {
        id: entries

        // we need a layout manager to make sure the occupied space of
        // SwipeListItem is removed when it's invisible
        header: RowLayout {
            width: parent.width

            Kirigami.SwipeListItem {
                visible: !backend.inSearchMode && backend.model.isRoot === false
                Layout.fillWidth: true

                onClicked: backend.model.selectParent()

                RowLayout {
                    Kirigami.Icon {
                        source: "folder"
                    }
                    Label {
                        Layout.fillWidth: true
                        text: ".."
                    }
                }
            }
        }

        model: DelegateModel {
            id: delegateModel
            model: backend.model
            delegate: DelegateChooser {
                role: "nodeType"

                DelegateChoice {
                    roleValue: "Entry"
                    delegate: EntryDelegate {
                        onCopyItemTriggered: backend.copyToClipboard(index,
                                                                     field)
                        onOpenUrlTriggered: backend.openUrl(index)
                    }
                }

                DelegateChoice {
                    roleValue: "Group"
                    delegate: GroupDelegate {
                        onActivated: {
                            backend.model.selectItem(index)
                        }
                    }
                }
            }
        }
    }

    onBackRequested: {
        backend.closeDatabase()
    }

    footer: ToolBar {
        Kirigami.ActionToolBar {
            actions: [
                Kirigami.Action {
                    displayComponent: Kirigami.SearchField {
                        id: searchField
                        onAccepted: backend.search(searchField.text)
                    }
                },
                Kirigami.Action {
                    icon.name: "lock"
                    text: 'Lock Database'
                    onTriggered: backend.closeDatabase()
                }
            ]
        }
    }
}
