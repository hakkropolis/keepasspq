import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.16 as Kirigami
import QtQml 2.15

Kirigami.Page {
    id: openDialogPage
    title: 'Open DB'

    FileDialog {
        id: fileDialog
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        nameFilters: ["KDBX Files (*.kdbx)", "All files (*)"]
    }

    Kirigami.FormLayout {
        width: openDialogPage.width

        Row {
            TextField {
                id: pathField
                text: fileDialog.fileUrl
                placeholderText: 'database file'
            }
            Button {
                icon.name: 'folder'
                onClicked: {
                    fileDialog.setVisible(true)
                }
            }
        }

        Row {
            TextField {
                id: passwordField
                placeholderText: 'password'
                echoMode: passwordFieldVisible.checked ? TextInput.Normal : TextInput.Password
            }
            Button {
                id: passwordFieldVisible
                icon.name: checked ? 'password-show-on' : 'password-show-off'
                checkable: true
                checked: false
            }
        }

        Button {
            text: 'Open DB'
            onClicked: {
                const error = backend.openDatabase(pathField.text,
                                                   passwordField.text)

                if (error !== '') {
                    message.showError(error)
                } else {
                    message.clear()
                }
            }
        }
    }

    function leave() {
        passwordField.text = ''
    }
}
