import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.2

import org.kde.kirigami 2.15 as Kirigami

Kirigami.SwipeListItem {
    id: listItem

    signal copyItemTriggered(int index, string field)
    signal openUrlTriggered(int index)

    RowLayout {
        Kirigami.Icon {}
        Label {
            Layout.fillWidth: true
            text: nodeData.name
        }
    }

    actions: [
        Kirigami.Action {
            icon.name: 'user'
            visible: nodeData.hasField('username')
            onTriggered: copyItemTriggered(index, 'username')
        },
        Kirigami.Action {
            icon.name: 'password-copy'
            visible: nodeData.hasField('password')
            onTriggered: copyItemTriggered(index, 'password')
        },
        Kirigami.Action {
            icon.name: 'link'
            visible: nodeData.hasField('url')
            onTriggered: openUrlTriggered(index)
        }
    ]
}
