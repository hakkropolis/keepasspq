import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.2

import org.kde.kirigami 2.15 as Kirigami

Kirigami.SwipeListItem {
    id: listItem

    signal activated(int index)

    onClicked: listItem.activated(index)

    RowLayout {
        Kirigami.Icon {
            source: "folder"
        }
        Label {
            Layout.fillWidth: true
            text: nodeData.name
        }
    }
}
