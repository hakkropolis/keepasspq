import contextlib

from PySide2.QtCore import (Qt, QObject, Signal, Slot, Property, 
                            QModelIndex, QAbstractItemModel)



class ValidationError(RuntimeError):
    pass


class Node(QObject):

    nodeChanged = Signal()

    def __init__(self, kp_node, parent=None):
        super().__init__()

        self.kp_node = kp_node
        self._parent = parent

    @Property(str, notify=nodeChanged)
    def name(self):
        return str(self.kp_node)

    @Property(str, notify=nodeChanged)
    def type(self):
        return self.__class__.__name__

    def parent(self):
        return self._parent

    def row(self):
        if self.parent() != None:
            return self.parent().row_of_child(self)
        return 0

    def __repr__(self):
        return f'<Node({self.name})>'


class Entry(Node):

    @Property(str, notify=Node.nodeChanged)
    def name(self):
        return self.kp_node.title

    @Property('QVariantList', notify=Node.nodeChanged)
    def fields(self):
        fields = []
        if self.kp_node.username:
            fields.append('username')
        if self.kp_node.password:
            fields.append('password')
        if self.kp_node.url:
            fields.append('url')
        return fields

    @Slot(str, result=bool)
    def hasField(self, fieldName):
        return getattr(self.kp_node, fieldName) is not None

    def data(self, field):
        return getattr(self.kp_node, field)


class Group(Node):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._children = []
        self._children_loaded = False

    @Property(str, notify=Node.nodeChanged)
    def name(self):
        return self.kp_node.name

    def row_of_child(self, child):
        if not self._children_loaded:
            self._load_children()

        return self._children.index(child)

    def child(self, row):
        if not self._children_loaded:
            self._load_children()

        with contextlib.suppress(IndexError):
            return self._children[row]

    def child_count(self):
        if not self._children_loaded:
            self._load_children()

        return len(self._children)

    def _load_children(self):
        for subgroup in self.kp_node.subgroups:
            child_node = Group(subgroup, self)
            self._children.append(child_node)

        for entry in self.kp_node.entries:
            child_node = Entry(entry, self)
            self._children.append(child_node)

        self._children_loaded = True


class SearchResult(Group):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pattern = ''

    def setPattern(self, value):
        self.pattern = value

    def _load_children(self):
        entries = self.kp_node.find_entries(title=self.pattern, regex=True, flags='i')
        self._children = [Entry(node) for node in entries]


class KeePassModel(QAbstractItemModel):

    NODE_TYPE = Qt.UserRole + 1
    NODE_DATA = Qt.UserRole + 2

    modelChanged = Signal()

    def __init__(self, connection):
        super().__init__()

        self._connection = connection

        self._in_search_mode = False
        self._search_node = SearchResult(self._connection)

        self._root = self._group_node = Group(self._connection.root_group)

    def enterSearchMode(self, value):
        if not self._in_search_mode:
            self._root = self._search_node
            self._in_search_mode = True

        self._search_node.setPattern(value)

    def clearSearch(self):
        self._root = self._group_node
        self._in_search_mode = False

    def roleNames(self):
        return {
            self.NODE_TYPE: b'nodeType',
            self.NODE_DATA: b'nodeData',
        }

    def getNode(self, index):
        if index.isValid():
            node = index.internalPointer()
            if node:
                return node
        return self._root

    def index(self, row, column, parent:QModelIndex = None) -> QModelIndex:
        if not parent:
            return QModelIndex()

        parent_node = self.getNode(parent)

        if (child_item := parent_node.child(row)):
            return self.createIndex(row, column, child_item)

        return QModelIndex()

    def parent(self, child:QModelIndex) -> QModelIndex:
        if not child.isValid():
            return QModelIndex()

        child_item = self.getNode(child)
        parent_item = child_item.parent()

        if parent_item == self._root:
            return QModelIndex()

        return self.createIndex(parent_item.row(), 0, parent_item)

    def rowCount(self, parent:QModelIndex = None) -> int:
        if parent.column() > 0:
            return 0

        parent_item = self.getNode(parent)

        return parent_item.child_count()

    def columnCount(self, parent:QModelIndex = None) -> int:
        return 1

    def data(self, index:QModelIndex, role) -> object:
        if not index.isValid():
            return None

        if role == self.NODE_TYPE:
            node = self.getNode(index)
            return node.type

        if role == self.NODE_DATA:
            node = self.getNode(index)
            return node

        return None
