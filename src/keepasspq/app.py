import dataclasses as dc
import os
import pathlib
import sys
import urllib.parse
import webbrowser

import pykeepass

from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtCore import (Qt, QUrl, QObject, Signal, Slot, Property, 
                            QAbstractProxyModel, QSortFilterProxyModel,
                            QIdentityProxyModel, QTimer, QThread, QRunnable,
                            QThreadPool,
                            QModelIndex, QAbstractItemModel)

from keepasspq import model, qml


DEBUG = os.environ.get('KPPQ_DEBUG') in ('1', 'yes', 'true')


@dc.dataclass(frozen=True)
class Level:

    index: QModelIndex
    node: model.Node


class MapToListProxy(QSortFilterProxyModel):

    parentChanged = Signal()

    def __init__(self, sourceModel, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setSourceModel(sourceModel)

        self._levels = [Level(QModelIndex(), model.Node('Root'))]
        self.layoutChanged.connect(self.parentChanged)

    @Property(bool, notify=parentChanged)
    def isRoot(self):
        return len(self._levels) == 1

    @Property('QVariantList', notify=parentChanged)
    def selectionLevels(self):
        return [level.node.name for level in self._levels]

    @Slot(int)
    def selectLevel(self, row):
        self._levels = self._levels[:row + 1]
        self.layoutChanged.emit()

    @Slot()
    def selectParent(self):
        self._levels.pop()
        self.layoutChanged.emit()

    @Slot(int)
    def selectItem(self, row):
        model = self.sourceModel()

        new_index = model.index(row, 0, self._levels[-1].index)

        node = model.getNode(new_index)
        if node.type == 'Group':
            self._levels.append(Level(new_index, node))
            self.layoutChanged.emit()

    def getNode(self, row):
        model = self.sourceModel()
        index = model.index(row, 0, self._levels[-1].index)
        return model.getNode(index)

    def mapFromSource(self, sourceIndex):
        return self.sourceModel().index(sourceIndex.row(), 0, self._levels[-1].index)

    def mapToSource(self, proxyIndex):
        return self.sourceModel().index(proxyIndex.row(), 0, self._levels[-1].index)


class Backend(QObject):

    databaseOpened = Signal()
    databaseClosed = Signal()

    modelUpdated = Signal()

    def __init__(self, clipboard):
        super().__init__()

        self._clipboard = clipboard

        self._path = None

        self._raw_model = None
        self._proxy = None

        self._in_search_mode = False

    @Property(str, notify=databaseOpened)
    def databaseName(self):
        return str(self._path.name)

    @Property(bool, notify=modelUpdated)
    def inSearchMode(self):
        return self._in_search_mode

    @Property(QObject, constant=False, notify=modelUpdated)
    def model(self):
        if self._in_search_mode:
            return self._raw_model
        else:
            return self._proxy

    @Slot(str, str, result=str)
    def openDatabase(self, path, password):
        if self._path:
            return 'db already open'

        if not path and DEBUG:
            path = 'test.kdbx'
            password = 'testpw'

        elif not path:
            return 'no path specified'

        url_data = urllib.parse.urlparse(path)
        path = pathlib.Path(urllib.parse.unquote(url_data.path))

        if not path.exists():
            return f'path {path} does not exist'

        try:
            connection = pykeepass.PyKeePass(path, password=password)
        except (pykeepass.exceptions.CredentialsError, FileNotFoundError):
            return 'Wrong file or credentials'

        self._path = path
        self._raw_model = model.KeePassModel(connection)
        self._proxy = MapToListProxy(self._raw_model)

        self.databaseOpened.emit()

    @Slot(result=str)
    def closeDatabase(self):
        self._path = None
        self._raw_model = None
        self._proxy = None

        self.databaseClosed.emit()

    @Slot(int, str)
    def copyToClipboard(self, row, itemName):
        data = self._getData(row, itemName)
        self._clipboard.setText(data)

    @Slot(int)
    def openUrl(self, row):
        data = self._getData(row, 'url')
        webbrowser.open(data)

    def _getData(self, row, fieldName):
        node = self.model.getNode(row)
        return node.data(fieldName)

    @Slot(str)
    def search(self, value):
        if value:
            self._raw_model.enterSearchMode(value)
            self._in_search_mode = True
            self.modelUpdated.emit()
        else:
            self._raw_model.clearSearch()
            self._in_search_mode = False
            self.modelUpdated.emit()


def main():
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()

    clipboard = app.clipboard()

    backend = Backend(clipboard)
    engine.rootContext().setContextProperty('backend', backend)

    engine.load(os.fspath(_get_file('qml/app.qml')))

    if not engine.rootObjects():
        print('no root objects')
        sys.exit(-1)

    sys.exit(app.exec_())


def _get_file(subpath=None):
    root = pathlib.Path(__file__).resolve().parent
    if hasattr(sys, 'frozen'):
        root = root.parent
    if subpath:
        return root / subpath
    return root
