# KeePassPQ

KeePassPQ is a Python and QML based application to open KeePass (v4)
databases on mobile devices. It mainly targets the kde plasma environment
(pinephone) and uses the [pykeepass](https://pypi.org/project/pykeepass/)
library.

Currently this application is fairly limited and more of a work in progress
until another Qt/Qml based convergent app is available or a higher maturity
is reached.

# Roadmap

1. Improve search functionality
1. Show details of single entries
1. (maybe) allow editing entries
1. (maybe) allow adding new groups and entries

# Install

## arch linux

As of now there are no versions so the PKGBUILD tracks the git HEAD.

```
curl https://gitlab.com/hakkropolis/keepasspq/-/raw/master/PKGBUILD > PKGBUILD
makepkg -sri
```

## other OSs

Currently, there are no other packages available. However, KeePassPQ can be
executed through python. Checkout the contribution section for a setup. Also
make sure to have the qml package kirigami2 installed.

After that just link the `keepasspq` binary from within the virtualenv to a
directory on your PATH variable. You can also copy the `keepasspq.desktop`
file and `keepasspq.png` (128x128) to your OS specific directories.


# Contribution

After cloning the repository you can use one of the following options.

## with poetry

```
poetry install
```

Then run it with

```
poetry run keepasspq
```

## with pure python

```
python -m venv <your_venv_name>
<your_venv_name>/bin/python setup.py develop
```

Then run it with

```
<your_venv_name>/bin/keepasspq
```

**NOTE**

If installing PySide2 into a virtualenv causes problems it is most likely
because it can't find kirigami. In that case you can install the pyside2 package
provided by your OS and configure the virtualenv to use global packages as well.
This can be done by setting `include-system-site-packages = true` inside
`<your_venv_name>/pyvenv.cfg`. To not shadow the system package make sure to
uninstall PySide2 from the virtualenv with `<your_venv_name>/bin/pip uninstall PySide2`.

You can use the following variables to simplify development

- to enable qml logging `QT_LOGGING_RULES=qml=true`
- to simulate mobile devices `QT_QUICK_CONTROLS_MOBILE=1`
- to quickly open the test keepass db `KPPQ_DEBUG=1`
