# -*- coding: utf-8 -*-
from setuptools import setup

package_dir = \
{'': 'src'}

packages = \
['keepasspq']

package_data = \
{'': ['*'], 'keepasspq': ['qml/*', 'qml/components/*']}

install_requires = \
['PySide2>=5.15.2,<6.0.0', 'pykeepass>=4.0.1,<5.0.0']

entry_points = \
{'console_scripts': ['keepasspq = keepasspq.app:main']}

setup_kwargs = {
    'name': 'keepasspq',
    'version': '0.1.0',
    'description': 'PySide2 and QML based frontend for keepass databases',
    'long_description': None,
    'author': 'Philipp Busch',
    'author_email': 'dev@hakkropolis.org',
    'maintainer': None,
    'maintainer_email': None,
    'url': None,
    'package_dir': package_dir,
    'packages': packages,
    'package_data': package_data,
    'install_requires': install_requires,
    'entry_points': entry_points,
    'python_requires': '>=3.7,<3.11',
}


setup(**setup_kwargs)
